import time
import sys
import pickle
import argparse
import random
from getpass import getpass
from pathlib import Path
import csv
from utils.conversion_tables import *
from utils.tools import bcolors as bc

class Config:
    input_file = Path("./utils/dict_concised_2014_20230926_no_break.csv")
    user_file = Path("./exercise_list.txt")
    missed_db = Path("./logs/missed.pkl")
    
    sp = "　"  # NOTE: this is a full-width space
    word_len = None  # set to any integers to limit word length
    common_level = (
        1  # most common spells are either 0 or 1; you may don't need to change this.
    )
    score_step = 5  # for every cfg.score_step you get one more smiley!


def recover_user_input(user_input):
    try:
        return "".join(ab2ch[ab] for ab in user_input.strip())
    except:
        return None


def find_in_db(w, full_data):
    for row in full_data:
        if w == row[0]:
            return row
    return None


def get_time_stamp():
    return time.strftime("%Y%m%d-%H%M")


def get_hhmmss(sec):
    if sec > 863999:
        return "[err] the timer has been over 23:59:59"
    return time.strftime("%H:%M:%S", time.gmtime(sec))


def update_missed_words(cfg, words):
    with open(cfg.missed_db, "wb") as f:
        pickle.dump(list(words), f)


def load_missed_list(cfg):
    missed_list = []
    with open(cfg.missed_db, "rb") as f:
        try:
            missed_list = pickle.load(f)
        except:
            print("[info] {cfg.missed_db} is empty")
    return missed_list


def run_test(cfg, test_data, mode=0):
    time_stamp = get_time_stamp()
    time_start = time.time()

    user_score = 0
    counter = 0

    missed_list = load_missed_list(cfg)

    while True:
        question = random.choice(test_data)

        word = question[0]
        common_score = int(question[5])
        spell = question[6]
        explanation = question[13]
        if cfg.word_len:
            if len(word) > cfg.word_len:
                continue
        if common_score > cfg.common_level:
            continue

        print(f">>> {word}")
        user_input = getpass(prompt="<<< ")

        spell_joined = spell.replace(" ", "").replace(cfg.sp, "")
        user_ans = user_input.replace(" ", "")

        answer = "".join(ch2ab[c] for c in spell_joined)

        if user_ans == "ccc":
            exercise_time = time.time() - time_start
            update_missed_words(cfg, missed_list)
            print(f"Your score: {user_score} with {counter} trials")
            print(f"Exercise time: {get_hhmmss(exercise_time)}")
            print(f"All the {len(missed_list)} missed words have been updated.")
            print("Have a nice day!")
            sys.exit()
        elif user_ans == answer:
            print(f"{bc.OKGREEN}{'😀'*(user_score//cfg.score_step+1)}{bc.ENDC}")
            print(f"{bc.UNDERLINE}{bc.OKBLUE}{spell}{bc.ENDC}")
            print(explanation)
            user_score += 1

            if missed_list != []:
                try:
                    missed_list.remove(word)
                except:
                    pass
            else:
                if mode == 1:
                    update_missed_words(cfg, missed_list)
                    print(
                        f"{bc.OKGREEN}Congrate! You've clean up missed words!{bc.ENDC}"
                    )
                    sys.exit()
        else:
            missed_list.append(word)
            user_ans = recover_user_input(user_input)
            print(
                f"Ans is {bc.UNDERLINE}{bc.OKBLUE}{spell}{bc.ENDC}; "
                f"you typed {bc.UNDERLINE}{bc.FAIL}{user_ans}{bc.ENDC}"
            )
            print(explanation)

        counter += 1


def main():
    cfg = Config
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--file", type=str, default=cfg.user_file)
    parser.add_argument("-m", "--mode", type=int, default=0)
    args = parser.parse_args()
    user_file = args.file
    exec_mode = args.mode

    exec_words = []
    match exec_mode:
        case 0:
            cfg.user_file = user_file
            print(f"Loading user defined file {cfg.user_file}...")
            with open(cfg.user_file, "r") as f:
                exec_words = f.read().splitlines()
        case 1:
            print(f"Loading missed words...")
            exec_words = load_missed_list(cfg)
            if exec_words == []:
                print(
                    f"{bc.OKBLUE}No missed words for you to exercise. Sorry.{bc.ENDC}"
                )
                sys.exit()

    # load full data from dictionary file
    full_data = []
    with open(cfg.input_file, "r") as f:
        full_data = list(csv.reader(f))

    # check the list
    print(f"{bc.OKGREEN}Loading data...{bc.ENDC}")
    test_data = []
    for w in exec_words:
        found = find_in_db(w, full_data)
        if found:
            test_data.append(found)
        else:
            print(f"{bc.WARNING}[info] cannot find {w}{bc.ENDC} in the dictionary")

    run_test(cfg, test_data, exec_mode)


if __name__ == "__main__":
    main()
